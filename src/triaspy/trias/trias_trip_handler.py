"""
Handler for TRIAS TripRequests
"""
from triaspy.model.options import TripRequestOptions
from triaspy.xml.xml_files import TRIAS_TRIP_REQUEST, VIA, STOP_POINT_REF, GEO_POSITION
from triaspy.request import request


class TRIASTripHandler:
    """
    Handler for TRIAS TripRequest
    """

    def __init__(self, url: str, requestor_reference: str, headers: dict):
        self.__url = url
        self.__requestor_reference = requestor_reference
        self.__headers = headers

    def get_trip_request(self, trip_options: TripRequestOptions):
        """
        Creates a TripRequest according to the given options
        :param trip_options: options on which journeys should get retrieved
        :return: trip request build according to the options
        """
        request_body = TRIAS_TRIP_REQUEST
        request_body = request_body.replace("$DEPARTURE_TIME", trip_options.get_departure_time())
        request_body = request_body.replace("$INCLUDE_FARES", str(trip_options.are_fares_included()).lower())

        if isinstance(trip_options.get_location_reference(), str):
            request_body = request_body.replace("$LOCATION_REF", STOP_POINT_REF)
            request_body = request_body.replace("$ID", trip_options.get_location_reference())
        else:
            request_body = request_body.replace("$LOCATION_REF", GEO_POSITION)
            request_body = request_body.replace("$LONGITUDE", trip_options.get_origin().get_longitude())
            request_body = request_body.replace("$LATITUDE", trip_options.get_origin().get_latitude())
        if isinstance(trip_options.get_destination(), str):
            request_body = request_body.replace("$LOCATION_REF", STOP_POINT_REF)
            request_body = request_body.replace("$ID", trip_options.get_destination())
        else:
            request_body = request_body.replace("$LOCATION_REF", GEO_POSITION)
            request_body = request_body.replace("$LONGITUDE", trip_options.get_destination()[0])
            request_body = request_body.replace("$LATITUDE", trip_options.get_destination()[1])
        if trip_options.get_via() is not None:
            request_body = request_body.replace("$VIA", VIA)
            request_body = request_body.replace("$VIA", trip_options.get_via()[0])
        else:
            request_body = request_body.replace("$VIA", "")
        request_body = request_body.replace("$TOKEN", self.__requestor_reference)
        return request_body

    def get_trips(self, trip_options: TripRequestOptions):
        """
        Return the journeys according to the given options
        :param trip_options: options on which journeys should get retrieved
        :return: trips according to the given options
        """
        request_body = self.get_trip_request(trip_options)
        return request(url=self.__url, request_body=request_body, headers=self.__headers)
