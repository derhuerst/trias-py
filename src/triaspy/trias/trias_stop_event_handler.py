"""
Handler for TRIAS Departure Endpoints
"""
from triaspy.model.options import StopEventRequestOptions
from triaspy.xml.xml_files import TRIAS_STOP_EVENT_REQUEST
from triaspy.request import request


class TRIASStopEventHandler:
    """
    Handler for TRIAS Departure Endpoints
    """

    def __init__(self, url: str, requestor_reference: str, headers: dict):
        self.__url = url
        self.__requestor_reference = requestor_reference
        self.__headers = headers

    def get_stop_event_request(self, stop_event_options: StopEventRequestOptions):
        """
        Creates the StopEvent Request according to the given options
        :param stop_event_options: options on which stop events should be gotten
        :return: the departures according to the given options
        """
        request_body = TRIAS_STOP_EVENT_REQUEST
        request_body = request_body.replace("$STATIO_NID", stop_event_options.get_station_id())
        request_body = request_body.replace("$TIME", stop_event_options.get_time())
        request_body = request_body.replace("$MAX_RESULTS", str(stop_event_options.get_max_results()))
        request_body = request_body.replace("$TOKEN", self.__requestor_reference)
        return request_body

    def get_stop_events(self, stop_event_options: StopEventRequestOptions):
        """
        Requests departures according to the given options
        :param stop_event_options: options on which departures should be gotten
        :return: the departures according to the given options
        """
        request_body = self.get_stop_event_request(stop_event_options)
        return request(url=self.__url, request_body=request_body, headers=self.__headers)
