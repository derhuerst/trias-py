"""
main module providing the TRIASClient
"""
import json
import importlib.resources as pkg_resources
from typing import Optional
from triaspy.trias.trias_stop_event_handler import TRIASStopEventHandler
from triaspy.model import options
from triaspy import configurations
from triaspy.trias.trias_trip_handler import TRIASTripHandler


def get_client(config_name: str, authorization_key: Optional[str]):
    """
    Offers a handy way to get a TRIAS-Client instance
    :param authorization_key: key to authorize when needed
    :param config_name: name of the config to load
    :return: a TRIAS-Client instance
    """
    config_filename = config_name + "-trias.json"
    config_file = pkg_resources.open_text(configurations, config_filename)
    configuration = json.load(config_file)
    url = configuration["options"]["endpoint"]
    headers = {"content-type": configuration["options"]["requestContentType"]}
    client_options = options.ClientOptions(url=url, requestor_ref=authorization_key, headers=headers)
    return TRIASClient(client_options)


class TRIASClient:
    """
    TRIASClient for communicating with a specific TRIAS-API
    """

    def __init__(self, client_options: options.ClientOptions):
        """
        Constructor of the Client
        :param client_options: options to configure the TRIASClient
        """
        if not client_options.requestor_ref:
            client_options.set_requestor_ref("")
        if not client_options.headers:
            client_options.set_headers({})
        self.departures_handler = \
            TRIASStopEventHandler(client_options.url, client_options.requestor_ref, client_options.headers)
        self.journey_handler = \
            TRIASTripHandler(client_options.url, client_options.requestor_ref, client_options.headers)

    def get_stop_events(self, departure_options: options.StopEventRequestOptions):
        """
        Returns the available departure based on specified options
        :param departure_options: the options for which the departure is requested
        :return: the available departures
        """
        return self.departures_handler.get_stop_events(departure_options)

    def get_trips(self, trip_options: options.TripRequestOptions):
        """
        returns the available trips based on specified options
        :param trip_options: the options for which the trip is requested
        :return: the available trips
        """
        return self.journey_handler.get_trips(trip_options)
