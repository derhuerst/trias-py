"""
Defined classes to model certain parts of the Requests as objects
"""
from typing import Union, Tuple


class Origin:
    """
    Origin as a representation of the Origin of e.g. a TripRequest
    """

    def __init__(self, location_reference: Union[str, Tuple[str, str]], departure_time: str):
        self.__location_reference: Union[str, Tuple[str, str]] = location_reference
        self.__departure_time: str = departure_time

    def get_location_reference(self):
        """
        Return the location reference
        :return: the location reference
        """
        return self.__location_reference

    def get_departure_time(self):
        """
        Return the departure time
        :return: the departure time
        """
        return self.__departure_time

    def get_longitude(self):
        """
        Returns the longitude of the geo-point
        :return: the longitude of the geo-point
        """
        return self.__location_reference[0]

    def get_latitude(self):
        """
        Returns the longitude of the geo-point
        :return: the longitude of the geo-point
        """
        return self.__location_reference[1]
