"""
This Module contains informal interfaces for parameter options
"""
from typing import List, Optional, Tuple, Union

from triaspy.model.trias_elements import Origin


class ClientOptions:
    """
    Informal Interface for options that can be given to a client
    """

    def __init__(self, url, requestor_ref, headers):
        self.url = url
        self.requestor_ref = requestor_ref
        self.headers = headers

    def set_requestor_ref(self, requestor_ref):
        """
        Updates the requestor_ref
        :param requestor_ref: updated requestor_ref
        """
        self.requestor_ref = requestor_ref

    def set_headers(self, headers):
        """
        Updates the __headers
        :param headers: updated __headers
        """
        self.headers = headers


class StopEventRequestOptions:
    """
    Informal Interface for options that can be given to a StopHandler
    """

    def __init__(self, station_id: str, time: str, max_results: int):
        self.__station_id = station_id
        self.__time = time
        self.__max_results = max_results

    def get_station_id(self):
        """
        Returns the station_id
        :return: the station_id
        """
        return self.__station_id

    def get_time(self):
        """
        Returns time-point option
        :return: time-point option
        """
        return self.__time

    def get_max_results(self):
        """
        Return maximum wanted results
        :return: maximum wanted results
        """
        return self.__max_results


class TripRequestOptions:
    """
    Informal Interface for options that can be given to a TripHandler
    """

    def __init__(self, origin: Origin, destination: Union[str, Tuple[str, str]],
                 trip_parameter: {}, via: Optional[List[str]] = None):
        self.__origin: Origin = origin
        self.__destination: str = destination
        self.__via: List[str] = via
        self.__trip_parameter: {} = trip_parameter

    def get_departure_time(self):
        """
        Returns the departure time of the origin
        :return: returns the departure time
        """
        return self.__origin.get_departure_time()

    def are_fares_included(self):
        """
        Returns whether fares should be included
        :return: whether fares should be included
        """
        return self.__trip_parameter["include_fares"]

    def get_location_reference(self):
        """
        Returns the location reference which can e.g. be a DHID
        :return: the location reference of the origin
        """
        return self.__origin.get_location_reference()

    def get_origin(self):
        """
        Return the origin
        :return: the origin
        """
        return self.__origin

    def get_destination(self):
        """
        Returns the destination of the Trip
        :return: the destination
        """
        return self.__destination

    def get_via(self):
        """
        Returns the via points
        :return: the list of via points
        """
        return self.__via
