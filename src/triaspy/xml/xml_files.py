"""
XML templates to build the requests
"""

TRIAS_TRIP_REQUEST = """<?xml version="1.0" encoding="UTF-8"?>
<Trias version="1.1" xmlns="http://www.vdv.de/trias" xmlns:siri="http://www.siri.org.uk/siri">
    <ServiceRequest>
        <siri:RequestTimestamp>2022-01-18T09:00:00</siri:RequestTimestamp>
        <siri:RequestorRef>$TOKEN</siri:RequestorRef>
        <RequestPayload>
            <TripRequest>
                <Origin>
                    <LocationRef>
                        $LOCATION_REF
                    </LocationRef>
                    <DepArrTime>$DEPARTURE_TIME</DepArrTime>
                </Origin>
                $VIA
                <Destination>
                    <LocationRef>
                        $LOCATION_REF
                    </LocationRef>
                </Destination>
                <Params>
                    <IncludeTurnDescription>true</IncludeTurnDescription>
                    <IncludeTrackSections>true</IncludeTrackSections>
                    <IncludeLegProjection>true</IncludeLegProjection>
                    <IncludeIntermediateStops>true</IncludeIntermediateStops>
                    <IncludeFares>$INCLUDE_FARES</IncludeFares>
                </Params>
            </TripRequest>
        </RequestPayload>
    </ServiceRequest>
</Trias>"""
VIA = """<Via>
    <ViaPoint>
        <StopPointRef>$VIA</StopPointRef>
    </ViaPoint>
</Via>"""
STOP_POINT_REF = """<StopPointRef>$ID</StopPointRef>"""
GEO_POSITION = """<GeoPosition>
    <Longitude>$LONGITUDE</Longitude>
    <Latitude>$LATITUDE</Latitude>
</GeoPosition>"""
TRIAS_STOP_EVENT_REQUEST = """<?xml version="1.0" encoding="UTF-8" ?>
<Trias version="1.1" xmlns="http://www.vdv.de/trias" xmlns:siri="http://www.siri.org.uk/siri" \
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
xsi:schemaLocation="https://raw.githubusercontent.com/VDVde/TRIAS/v1.1/Trias.xsd">
    <ServiceRequest>
        <siri:RequestTimestamp>2022-01-18T09:00:00</siri:RequestTimestamp>
        <siri:RequestorRef>$TOKEN</siri:RequestorRef>
        <RequestPayload>
            <StopEventRequest>
                <Location>
                    <LocationRef>
                        <StopPointRef>$STATION_ID</StopPointRef>
                    </LocationRef>
                    <DepArrTime>$TIME</DepArrTime>
                </Location>
                <Params>
                    <NumberOfResults>$MAX_RESULTS</NumberOfResults>
                    <StopEventType>departure</StopEventType>
                    <IncludePreviousCalls>false</IncludePreviousCalls>
                    <IncludeOnwardCalls>false</IncludeOnwardCalls>
                    <IncludeRealtimeData>true</IncludeRealtimeData>
                </Params>
            </StopEventRequest>
        </RequestPayload>
    </ServiceRequest>
</Trias>"""
