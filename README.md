# TRIAS-py

A python client for TRIAS APIs.

Currently, it is still in early work-in-progress.

## Getting Started

1. Clone this repository
2. Install the requirements form the requirements.txt: `pip install -r requirements.txt`

### Prerequisites

You need a recent python version installed. Development is done with Python 3.10, no other python versions are tested 
yet.
Use the following command in a terminal to check your python version:
```
python --version
# Output: Python 3.10.5
```

### Installing

1. Clone this repository
2. Install the requirements form the requirements.txt: `pip install -r requirements.txt`
3. Install the module itself: `pip install -e .`

## Running the tests

Tests will be placed in the `test` directory. To execute them install pytest, install the repository locally and run 
them with pytest. Note a proper API-KEY needs to be offered in an environment variable called `APIKEY`
```cmd
pip install pytest
pip install -e .
APIKEY= YOUR-API-KEY pytest ./test
```

### And coding style tests

This projects uses pylint to check for coding style. Therefore, install pylint and execute it:

```
pip install pylint
pylint src --max-line-length=120
```

## Deployment

Download the release from the [Releases](https://gitlab.com/motis-datagon/trias-py/-/releases) page, when it's 
available.

## Built With

* [requests](https://github.com/psf/requests) - a simple, yet elegant, HTTP library
* [xmltodict](https://github.com/martinblech/xmltodict) - a Python module that makes working with XML feel like you are 
working with JSON

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull 
requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the 
[tags on this repository](https://github.com/your/project/tags). 

## What is TRIAS?

TRIAS stands for "Travellors Realtime Information and Advisory Standard", has been developed in scope
of the research and standardisation project for public transport "IP-KOM-ÖV" and was then introduced in 2014 as a 
standardized specification by the VDV 
([Verband Deutscher Verkehrsunternehmen](https://de.wikipedia.org/wiki/Verband_Deutscher_Verkehrsunternehmen)). 
TRIAS offers a wide-range list of functionalities, including station / location search, realtime departures, navigation,
ticket price calculation, malfunction reportings, and so on.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* This Client and it's structure is heavily inspired by [trias-client](https://github.com/andaryjo/trias-client), a 
* Node.js client for TRIAS APIs.

## Related resources

Dou you want to develop your own TRIAS client? Here are some resources:
- [VDV 431-2 EKAP-Schnittstellenbeschreibung (german)](https://www.vdv.de/ip-kom-oev.aspx)
- [VDV TRIAS XML specification](https://github.com/VDVde/TRIAS)
- [TRIAS implementation example in PHP](https://www.vrn.de/opendata/node/118)
- [TRIAS request examples (german)](https://www.verbundlinie.at/fahrplan/rund-um-den-fahrplan/link-zum-fahrplan)
